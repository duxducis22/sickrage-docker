# Load ubuntu 15 to get a later python
FROM ubuntu:vivid

RUN apt-get update && apt-get install -y \
  git-core \
  python \
  python-cheetah \
  tar

EXPOSE 8081

# Setup sickrage directories
RUN mkdir /sickrage
RUN mkdir /config

# Setup sickrage s6 service 
RUN mkdir -p /etc/services.d/sickrage
COPY run.sh /etc/services.d/sickrage/run
RUN ln -s /bin/true /etc/services.d/sickrage/finish


# Setup pull init script
COPY pull.sh /etc/cont-init.d/

# Download and extract s6 init
ADD https://github.com/just-containers/s6-overlay/releases/download/v1.10.0.3/s6-overlay-amd64.tar.gz /tmp/
RUN tar xzf /tmp/s6-overlay-amd64.tar.gz -C /

ENTRYPOINT ["/init"]