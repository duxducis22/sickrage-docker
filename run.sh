#!/bin/bash
# Start sickrage in the foreground. Use exec to properly pass SIG
exec /sickrage/SickBeard.py --nolaunch --datadir=/config
